Rails.application.routes.draw do
  resources :items
  post '/items/:id/sign_up', to: 'items#sign_up', as: 'sign_up'
  post '/items/:id/password', to: 'items#input_password', as: 'item_input_password'
  get '/emails/:id/:validation_token', to: 'emails#validate', as: 'validate'
  root to: 'items#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
