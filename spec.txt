+ A Creator creates an Item. 
  + An Item has the following properties:
    + A Name.							
    + A Description.
    + A Threshold. 
    + A Password, known only to Password-Bearers.
    + A Success Message.
+ An Email belongs to an Item, is unique per Item, and has the following properties:
  + An Address.
  + A Verification Key, which is randomly generated and, barring a security breach, does not expose the Email.
  + A Verification Status, which is boolean.
  + When an Item has yet to reach its Threshold:
    + Password-Bearers may:
      + See how many Verified Signups the Item has.
      + Edit the Success Message. (They may not edit the Name, Description, Threshold, or Password.)
    + Users may:
      + Sign up for the Item:
        + Input an email address, to which a Verification Message is sent, containing a Verification Key and a URL that may be followed to apply it.
      + Verify their signup by applying the Verification Key.
  - Once the Item has accumulated a number of Verified Signups equal to or greater than the Threshold:
    + The Success Message is sent to all Verified Signups, 
    - along with a Survey Invitation, consisting of a free-form text box, the contents of which are stored in Survey Results. This Survey Invitation is linked to the Verification Key.
    - Password-Bearers may:
      - View the Survey Results.
      - View what percentage of Verified Signups submitted Survey Results.

+ Creators should be able to set visibility of signups and threshold.
- Creators should be able to toggle allowing users to input their own threshold.


Current problems:
+ Threshold accepts negative values.
- It shouldn't let you sign up twice with the same email.
  - But should maybe fail silently for privacy reasons.
  - Dunno how to handle this.
  - Shouldn't doublesend verification emails.
- Items should have end dates. When the end date comes around, it's no longer open to signup but it's still visible.
- People might be confused as to what to link to to invite people.
+ Sort newest to oldest.
- Add pagination.
+ Add public vs nonpublic so your parties don't show up in index.
  - Need to change URL param so private items can't get weeved.