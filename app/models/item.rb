class Item < ApplicationRecord
	has_many :emails
	validates :threshold, numericality: {greater_than: 0}

	def validated_signups
		self.emails.where(validated: true)
	end

	def succeeded?
		if self.validated_signups.count >= self.threshold
			self.validated_signups.where(sent_success_msg: false).each do |email|
				email.send_success_msg 
			end
			return true
		end
		return false
	end

	def self.hash(password)
		Digest::SHA256.base64digest(password || '')
	end
end
