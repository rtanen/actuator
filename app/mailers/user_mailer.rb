class UserMailer < ApplicationMailer
	default from: 'coordinationtoolthrowaway@gmail.com'

	def validation_email(email)
		@email = email
		@url = validate_url(@email.id, @email.validation_token)
		@item = email.item
		mail(to: @email.email, subject: "Validation")
	end

	def success_email(email)
		item = email.item
		@success_msg = item.success_msg 
		mail(to: email.email, subject: "Threshold met: #{item.title}")
	end
end