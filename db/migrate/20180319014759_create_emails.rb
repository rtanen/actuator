class CreateEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :emails do |t|
    	t.string :email
    	t.boolean :validated
    	t.string :validation_token
    	t.integer :item_id
    end
    add_foreign_key :emails, :items
  end
end
