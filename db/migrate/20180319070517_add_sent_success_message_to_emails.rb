class AddSentSuccessMessageToEmails < ActiveRecord::Migration[5.1]
  def change
  	add_column :emails, :sent_success_msg, :boolean, null: false, default: false
  end
end
