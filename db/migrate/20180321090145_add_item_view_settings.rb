class AddItemViewSettings < ActiveRecord::Migration[5.1]
  def change
  	add_column :items, :show_threshold, :boolean, null: false, default: true
  	add_column :items, :show_verified_signups, :boolean, null: false, default: false
  	add_column :items, :public, :boolean, null: false, default: true
  end
end
